package com.revolut.model;

import org.junit.Test;
import com.revolut.exception.MoneyLeftOverException;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static com.revolut.model.Currency.*;

public class AccountTest {

    @Test
    public void shouldBeZeroAmountWhenNewAccountIsCreated() {
        //given
        Account account = Account.builder()
                .accountNumber("1").owner("John Wick").currency(USD)
                .build();

        //then
        assertThat(account.getBalance()).isEqualByComparingTo(BigDecimal.ZERO);
    }

    @Test
    public void shouldDepositMoney() {
        //given
        Account account = Account.builder()
                .accountNumber("1").owner("John Wick").currency(USD)
                .build();
        //when
        account.deposit(BigDecimal.valueOf(1050));

        //then
        assertThat(account.getBalance()).isEqualByComparingTo(BigDecimal.valueOf(1050));
    }

    @Test
    public void shouldWithdrawMoney() {
        //given
        Account account = Account.builder()
                .accountNumber("2").owner("Will Smith").currency(PLN)
                .build();
        account.deposit(BigDecimal.valueOf(500));
        assertThat(account.getBalance()).isEqualByComparingTo(BigDecimal.valueOf(500));

        //when
        account.withdraw(BigDecimal.valueOf(300));

        //then
        assertThat(account.getBalance()).isEqualByComparingTo(BigDecimal.valueOf(200));
    }

    @Test(expected = MoneyLeftOverException.class)
    public void shouldThrowExceptionIfWithdrawMuchMoneyThanBalance() {
        //given
        Account account = Account.builder()
                .accountNumber("3").owner("Johnny Depp") .currency(EURO)
                .build();
        account.deposit(BigDecimal.valueOf(100));
        assertThat(account.getBalance()).isEqualByComparingTo(BigDecimal.valueOf(100));

        //when
        account.withdraw(BigDecimal.valueOf(150));
    }

    @Test
    public void shouldNotThrowExceptionIfWithdrawMoneyEqualsToBalance() {
        //given
        Account account = Account.builder()
                .accountNumber("4").owner("Johnny Depp") .currency(EURO)
                .build();
        assertThat(account.getBalance()).isEqualByComparingTo(BigDecimal.valueOf(0));
        account.deposit(BigDecimal.valueOf(100));
        assertThat(account.getBalance()).isEqualByComparingTo(BigDecimal.valueOf(100));

        //when
        account.withdraw(BigDecimal.valueOf(100));
        assertThat(account.getBalance()).isEqualByComparingTo(BigDecimal.valueOf(0));
    }
}
