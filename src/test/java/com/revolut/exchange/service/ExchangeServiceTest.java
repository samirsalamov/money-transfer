package com.revolut.exchange.service;

import com.revolut.model.Rate;
import com.revolut.storage.service.DataSource;
import com.revolut.storage.service.MemoryDataSource;
import org.junit.Test;

import java.util.List;

import static com.revolut.model.Currency.*;
import static org.assertj.core.api.Assertions.assertThat;

public class ExchangeServiceTest {

    private DataSource dataSource = new MemoryDataSource<>();
    private ExchangeService underTest = new ExchangeServiceImpl(dataSource);

    @Test
    public void shouldAddNewRate() {
        //given
        Rate rate = Rate.builder().from(USD).to(PLN).rate(3.75).build();

        //when
        rate = underTest.addRate(rate);

        //then
        assertThat(rate).isNotNull();

        List<Rate> rates = underTest.getRates();
        assertThat(rates).contains(rate);
    }

    @Test
    public void shouldFetchExistingRate() {
        //given
        Rate rate = Rate.builder().from(USD).to(PLN).rate(3.75).build();
        underTest.addRate(rate);

        //when
        rate = underTest.getRate(USD + "-" + PLN);

        //then
        assertThat(rate).isNotNull();
    }

    @Test
    public void shouldFetchAllExistingRates() {
        //given
        Rate rate1 = underTest.addRate(Rate.builder().from(USD).to(PLN).rate(3.75).build());
        Rate rate2 = underTest.addRate(Rate.builder().from(USD).to(AZN).rate(1.72).build());
        Rate rate3 = underTest.addRate(Rate.builder().from(EURO).to(USD).rate(1.12).build());

        //when
        List<Rate> rates = underTest.getRates();

        //then
        assertThat(rates).contains(rate1, rate2, rate3);
    }
}
