package com.revolut.account.service;

import org.junit.Test;
import com.revolut.model.Account;
import com.revolut.storage.service.DataSource;
import com.revolut.storage.service.MemoryDataSource;


import java.math.BigDecimal;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static com.revolut.model.Currency.*;

public class AccountServiceTest {

    private DataSource dataSource = new MemoryDataSource<>();
    private AccountService underTest = new AccountServiceImpl(dataSource);

    @Test
    public void shouldAddNewAccount() {
        //given
        Account dto = Account.builder().owner("Samir Salamov").currency(USD).build();

        //when
        Account newCreatedAccount = underTest.createAccount(dto);

        //then
        assertThat(newCreatedAccount.getAccountNumber()).isNotNull();
        assertThat(newCreatedAccount.getBalance()).isEqualTo(BigDecimal.ZERO);
        assertThat(newCreatedAccount.getOwner()).isEqualTo("Samir Salamov");
        assertThat(newCreatedAccount.getCurrency()).isEqualTo(USD);
    }

    @Test
    public void shouldReturnCorrectBalance() {
        //given
        Account dto = Account.builder().owner("Javier Bardem").currency(EURO).build();

        Account newCreatedAccount = underTest.createAccount(dto);
        underTest.deposit(newCreatedAccount.getAccountNumber(), BigDecimal.valueOf(100));
        underTest.withdraw(newCreatedAccount.getAccountNumber(), BigDecimal.valueOf(50));
        underTest.deposit(newCreatedAccount.getAccountNumber(), BigDecimal.valueOf(400));

        //when
        Account updatedAccount = underTest.getAccount(newCreatedAccount.getAccountNumber());

        //then
        assertThat(updatedAccount.getBalance()).isEqualTo(BigDecimal.valueOf(450));
        assertThat(updatedAccount.getOwner()).isEqualTo("Javier Bardem");
        assertThat(updatedAccount.getCurrency()).isEqualTo(EURO);
    }

    @Test
    public void shouldDepositCorrectlyWithinConcurrency() throws Exception {
        //given
        Account dto = Account.builder().owner("Samir Salamov").currency(PLN).build();
        Account newCreatedAccount = underTest.createAccount(dto);

        //when
        ExecutorService executorService = Executors.newFixedThreadPool(20);

        for(int i = 1; i <= 100; i++) {
            executorService.execute(() -> newCreatedAccount.deposit(BigDecimal.valueOf(100)));
        }
        executorService.shutdown();
        try {
            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException ex) {
        }
        //then
        Account updatedAccount = underTest.getAccount(newCreatedAccount.getAccountNumber());
        assertThat(updatedAccount.getBalance()).isEqualByComparingTo(BigDecimal.valueOf(10_000));
    }

    @Test
    public void shouldWithdrawnCorrectlyWithinConcurrency() throws Exception {
        //given
        Account dto = Account.builder().owner("Samir Salamov").currency(PLN).build();
        Account newCreatedAccount = underTest.createAccount(dto);
        newCreatedAccount.deposit(BigDecimal.valueOf(10_000));

        //when
        ExecutorService executorService = Executors.newFixedThreadPool(20);

        for(int i = 1; i <= 100; i++) {
            executorService.execute(() -> newCreatedAccount.withdraw(BigDecimal.valueOf(100)));
        }
        executorService.shutdown();
        try {
            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException ex) {
        }
        //then
        Account updatedAccount = underTest.getAccount(newCreatedAccount.getAccountNumber());
        assertThat(updatedAccount.getBalance()).isEqualByComparingTo(BigDecimal.valueOf(0));
    }
}
