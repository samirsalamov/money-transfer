package com.revolut.transfer.service;

import com.revolut.model.TransferDTO;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import com.revolut.account.service.AccountService;
import com.revolut.exception.AccountNotFoundException;
import com.revolut.exception.MoneyLeftOverException;
import com.revolut.exchange.service.ExchangeService;
import com.revolut.model.Account;
import com.revolut.account.service.AccountServiceImpl;
import com.revolut.exchange.service.ExchangeServiceImpl;
import com.revolut.model.Rate;
import com.revolut.storage.service.DataSource;
import com.revolut.storage.service.MemoryDataSource;
import org.junit.runners.MethodSorters;

import java.math.BigDecimal;

import static com.revolut.model.Currency.*;
import static org.assertj.core.api.Assertions.assertThat;

public class TransferServiceTest {

    private static DataSource dataSource = new MemoryDataSource();
    private static AccountService accountService = new AccountServiceImpl(dataSource);
    private static ExchangeService exchangeService = new ExchangeServiceImpl(dataSource);
    private static TransferService underTest = new TransferServiceImpl(accountService, exchangeService, dataSource);

    private static Account sourceAccount, destinationAccount;

    @BeforeClass
    public static void setUp() {
        Account sourceAccountDTO = Account.builder().currency(EURO).owner("Jhonny Depp").build();
        sourceAccount = accountService.createAccount(sourceAccountDTO);

        Account destinationAccountDTO = Account.builder().currency(PLN).owner("Samir Salamov").build();
        destinationAccount = accountService.createAccount(destinationAccountDTO);
    }

    private void resetBalance(Account account) {
        account.withdraw(account.getBalance());
    }

    @Test
    public void shouldTransferMoneyIfRateNotFound() {
        //given
        resetBalance(sourceAccount);
        accountService.deposit(sourceAccount.getAccountNumber(), BigDecimal.valueOf(500));

        resetBalance(destinationAccount);

        //when
        boolean result = underTest.transfer(TransferDTO.builder()
                .sourceAccount(sourceAccount.getAccountNumber())
                .destinationAccount(destinationAccount.getAccountNumber())
                .amount(BigDecimal.valueOf(100))
                .currency(AZN).build());

        Account updatedSourceAccount = accountService.getAccount(sourceAccount.getAccountNumber());
        Account updatedDestinationAccount = accountService.getAccount(destinationAccount.getAccountNumber());

        //then
        assertThat(result).isTrue();
        assertThat(updatedSourceAccount.getBalance()).isEqualByComparingTo(BigDecimal.valueOf(400));
        assertThat(updatedDestinationAccount.getBalance()).isEqualByComparingTo(BigDecimal.valueOf(100));
    }

    @Test
    public void shouldTransferMoneyIfRateFound() {
        //given
        exchangeService.addRate(Rate.builder().from(EURO).to(PLN).rate(4.28).build());
        resetBalance(sourceAccount);
        accountService.deposit(sourceAccount.getAccountNumber(), BigDecimal.valueOf(500));

        resetBalance(destinationAccount);

        //when
        boolean result = underTest.transfer(TransferDTO.builder()
                .sourceAccount(sourceAccount.getAccountNumber())
                .destinationAccount(destinationAccount.getAccountNumber())
                .amount(BigDecimal.valueOf(100))
                .currency(EURO).build());

        Account updatedSourceAccount = accountService.getAccount(sourceAccount.getAccountNumber());
        Account updatedDestinationAccount = accountService.getAccount(destinationAccount.getAccountNumber());

        //then
        assertThat(result).isTrue();
        assertThat(updatedSourceAccount.getBalance()).isEqualByComparingTo(BigDecimal.valueOf(400));
        assertThat(updatedDestinationAccount.getBalance()).isEqualByComparingTo(BigDecimal.valueOf(428));
    }

    @Test
    public void shouldTransferMoneyIfCurrencyDifferentFromAccounts() {
        //given
        exchangeService.addRate(Rate.builder().from(USD).to(EURO).rate(0.87).build());
        exchangeService.addRate(Rate.builder().from(USD).to(PLN).rate(3.75).build());
        resetBalance(sourceAccount);
        accountService.deposit(sourceAccount.getAccountNumber(), BigDecimal.valueOf(500));

        resetBalance(destinationAccount);

        //when
        boolean result = underTest.transfer(TransferDTO.builder()
                .sourceAccount(sourceAccount.getAccountNumber())
                .destinationAccount(destinationAccount.getAccountNumber())
                .amount(BigDecimal.valueOf(100))
                .currency(USD).build());

        Account updatedSourceAccount = accountService.getAccount(sourceAccount.getAccountNumber());
        Account updatedDestinationAccount = accountService.getAccount(destinationAccount.getAccountNumber());

        //then
        assertThat(result).isTrue();
        assertThat(updatedSourceAccount.getBalance()).isEqualByComparingTo(BigDecimal.valueOf(413));
        assertThat(updatedDestinationAccount.getBalance()).isEqualByComparingTo(BigDecimal.valueOf(375));
    }

    @Test(expected = MoneyLeftOverException.class)
    public void shouldThrowExceptionIfSourceAccountLeftOutOfMoney() {
        //given
        Account sourceAccountDTO = Account.builder().currency(AZN).owner("Samir Salamov").build();
        Account sourceAccount = accountService.createAccount(sourceAccountDTO);

        Account destinationAccountDTO = Account.builder().currency(EURO).owner("Jhonny Depp").build();
        Account destinationAccount = accountService.createAccount(destinationAccountDTO);

        //when
        underTest.transfer(
                TransferDTO.builder()
                        .sourceAccount(sourceAccount.getAccountNumber())
                        .destinationAccount(destinationAccount.getAccountNumber())
                        .amount(sourceAccount.getBalance().add(BigDecimal.valueOf(50)))
                        .currency(AZN).build());
    }

    @Test(expected = AccountNotFoundException.class)
    public void shouldThrowExceptionIfSourceAccountNotFound() {
        //given
        Account destinationAccountDTO = Account.builder().currency(EURO).owner("Jhonny Depp").build();
        Account destinationAccount = accountService.createAccount(destinationAccountDTO);

        //when
        underTest.transfer(
                TransferDTO.builder()
                        .sourceAccount("not_existing_account_number")
                        .destinationAccount(destinationAccount.getAccountNumber())
                        .amount(BigDecimal.valueOf(50))
                        .currency(AZN).build()
        );
    }

    @Test(expected = AccountNotFoundException.class)
    public void shouldThrowExceptionIfDestinationAccountNotFound() {
        //given
        Account sourceAccountDTO = Account.builder().currency(AZN).owner("Samir Salamov").build();
        Account sourceAccount = accountService.createAccount(sourceAccountDTO);

        //when
        underTest.transfer(
                TransferDTO.builder()
                        .sourceAccount(sourceAccount.getAccountNumber())
                        .destinationAccount("not_existing_account_number")
                        .amount(BigDecimal.valueOf(50))
                        .currency(AZN).build());
    }
}
