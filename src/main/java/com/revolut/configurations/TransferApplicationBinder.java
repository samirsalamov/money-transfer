package com.revolut.configurations;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import com.revolut.account.service.AccountService;
import com.revolut.exchange.service.ExchangeService;
import com.revolut.transfer.service.TransferService;
import com.revolut.account.service.AccountServiceImpl;
import com.revolut.exchange.service.ExchangeServiceImpl;
import com.revolut.transfer.service.TransferServiceImpl;
import com.revolut.storage.service.DataSource;
import com.revolut.storage.service.MemoryDataSource;

import javax.inject.Singleton;

public class TransferApplicationBinder extends AbstractBinder {
    @Override
    protected void configure() {

        bind(AccountServiceImpl.class).to(AccountService.class);
        bind(ExchangeServiceImpl.class).to(ExchangeService.class);
        bind(TransferServiceImpl.class).to(TransferService.class);

        //could be bind different kind of DataSource such Database
        bind(MemoryDataSource.class).to(DataSource.class).in(Singleton.class);
    }
}