package com.revolut.configurations;

import com.revolut.exception.ExceptionMapperImpl;
import org.glassfish.jersey.server.ResourceConfig;

public class TransferApplication extends ResourceConfig {
    public TransferApplication() {
        register(new ExceptionMapperImpl());
        register(new TransferApplicationBinder());
        packages(true, "com.revolut.api");
    }
}
