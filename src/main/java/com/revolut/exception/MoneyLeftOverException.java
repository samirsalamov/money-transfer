package com.revolut.exception;

public class MoneyLeftOverException extends RuntimeException {

    public MoneyLeftOverException(String errorMessage) {
        super(errorMessage);
    }
}
