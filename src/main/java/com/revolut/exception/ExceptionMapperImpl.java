package com.revolut.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ExceptionMapperImpl implements ExceptionMapper<Exception> {

    @Override
    public Response toResponse(Exception ex) {
        if (ex instanceof AccountNotFoundException) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(new ProjectStatus(ex.getMessage())).build();
        } else if (ex instanceof MoneyLeftOverException) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(new ProjectStatus(ex.getMessage())).build();
        } else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(new ProjectStatus("Something went wrong, please check system logs!")).build();
        }
    }

    class ProjectStatus {
        private String errorMessage;

        public ProjectStatus(String errorMessage) {
            this.errorMessage = errorMessage;
        }

        public String getErrorMessage() {
            return errorMessage;
        }
    }
}
