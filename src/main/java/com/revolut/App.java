package com.revolut;

/**
 * A class responsible for Server startup & shutdown life cycle
 */
public class App {
    public static void main(String[] args) throws Exception {
        final JettyServer jettyServer = new JettyServer();
        jettyServer.start();
    }
}