package com.revolut.utils;

public class AppVariables {
    public static final String ACCOUNTS_TABLE_NAME = "ACCOUNTS";

    public static final String RATES_TABLE_NAME = "RATES";

    public static final String TRANSACTIONS_TABLE_NAME = "TRANSACTIONS";
}
