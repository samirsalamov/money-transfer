package com.revolut.utils;

import java.util.Random;

public class IDGenerator {

    public static class AccountIDGenerator {

        public static String randomAccountId() {
            Random rand = new Random();
            StringBuilder card = new StringBuilder("RVL");
            for (int i = 0; i < 14; i++)
            {
                int n = rand.nextInt(10) + 0;
                card.append(Integer.toString(n));
            }
            return card.toString();
        }
    }

    public static class TransactionIDGenerator {

        public static String randomTransactionId() {
            Random rand = new Random();
            StringBuilder card = new StringBuilder("TR");
            for (int i = 0; i < 14; i++)
            {
                int n = rand.nextInt(10) + 0;
                card.append(Integer.toString(n));
            }
            return card.toString();
        }
    }
}
