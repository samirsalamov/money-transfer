package com.revolut.api;

import com.revolut.model.TransferDTO;
import com.revolut.transfer.service.TransferService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;

@Path("/transfer")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TransferServiceController {

    private TransferService transferService;

    @Inject
    TransferServiceController(TransferService transferService) {
        this.transferService = transferService;
    }

    @POST
    public boolean transfer(TransferDTO transferDto) {
        return transferService.transfer(transferDto);
    }
}
