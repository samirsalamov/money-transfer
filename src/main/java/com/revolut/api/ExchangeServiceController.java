package com.revolut.api;

import com.revolut.exchange.service.ExchangeService;
import com.revolut.model.Rate;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/exchanges")
@Produces(MediaType.APPLICATION_JSON)
public class ExchangeServiceController {

    private ExchangeService exchangeService;

    @Inject
    public ExchangeServiceController(ExchangeService exchangeService) {
        this.exchangeService = exchangeService;
    }

    @GET
    public List<Rate> getRates() {
        return exchangeService.getRates();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Rate addRate(Rate rate) {
        return exchangeService.addRate(rate);
    }
}
