package com.revolut.api;

import com.revolut.exception.AccountNotFoundException;
import com.revolut.model.Account;
import com.revolut.account.service.AccountService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;

@Path("/accounts")
@Produces(MediaType.APPLICATION_JSON)
public class AccountServiceController {

    private AccountService accountService;

    @Inject
    AccountServiceController(AccountService accountService) {
        this.accountService = accountService;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Account createAccount(Account dto) {
        return accountService.createAccount(dto);
    }

    @GET
    @Path("/{accountNumber}")
    public Account viewAccount(@PathParam("accountNumber") String accountNumber) {
        return accountService.getAccount(accountNumber);
    }

    @POST
    @Path("/deposit")
    public boolean deposit(@FormParam("accountNumber") String accountNumber, @FormParam("amount") BigDecimal amount) {
        return accountService.deposit(accountNumber, amount);
    }

    @POST
    @Path("/withdraw")
    public boolean withdraw(@FormParam("accountNumber") String accountNumber, @FormParam("amount") BigDecimal amount) {
        return accountService.withdraw(accountNumber, amount);
    }
}
