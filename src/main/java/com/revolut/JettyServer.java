package com.revolut;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;

import static org.eclipse.jetty.servlet.ServletContextHandler.NO_SESSIONS;

public class JettyServer {
    private final int port = 8888;
    private final Server jettyServer = new Server(port);

    public void start() {
        final ServletContextHandler contextHandler = new ServletContextHandler(NO_SESSIONS);
        contextHandler.setContextPath("/api");

        jettyServer.setHandler(contextHandler);

        final ServletHolder servletHolder = contextHandler.addServlet(ServletContainer.class, "/*");
        servletHolder.setInitOrder(0);
        servletHolder.setInitParameter("javax.ws.rs.Application", "com.revolut.configurations.TransferApplication");

        try {
            jettyServer.start();
            jettyServer.join();
        } catch (Exception ex) {
            System.exit(1);
        } finally {
            stop();
        }
    }

    public void stop() {
        jettyServer.destroy();
    }
}