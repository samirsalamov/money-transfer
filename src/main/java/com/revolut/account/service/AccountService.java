package com.revolut.account.service;

import com.revolut.model.Account;

import java.math.BigDecimal;

public interface AccountService {
    public Account getAccount(String accountNumber);
    public Account createAccount(Account dto);
    public boolean deposit(String accountNumber, BigDecimal amount);
    public boolean withdraw(String accountNumber, BigDecimal amount);
}
