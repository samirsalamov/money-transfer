package com.revolut.account.service;

import com.revolut.exception.AccountNotFoundException;
import com.revolut.model.Account;
import com.revolut.model.Entity;
import com.revolut.storage.service.DataSource;

import javax.inject.Inject;
import java.math.BigDecimal;
import org.apache.log4j.Logger;
import static com.revolut.utils.AppVariables.*;
import static com.revolut.utils.IDGenerator.AccountIDGenerator.*;

public class AccountServiceImpl implements AccountService {

    private static final Logger LOGGER = Logger.getLogger(AccountServiceImpl.class.getName());

    private DataSource dataSource;

    @Inject
    public AccountServiceImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Account getAccount(String accountNumber) {
        Entity entity = dataSource.getEntity(accountNumber, ACCOUNTS_TABLE_NAME);
        if (entity == null) {
            throw new AccountNotFoundException(String.format("Account['%s'] is not found!", accountNumber));
        }
        return (Account) entity;
    }

    @Override
    public Account createAccount(Account dto) {
        Account account = Account.builder()
                .accountNumber(getUniqueAccountNumber())
                .currency(dto.getCurrency())
                .owner(dto.getOwner())
                .build();
        return (Account)dataSource.addEntity(account);
    }

    private String getUniqueAccountNumber() {
        String accountID = randomAccountId();
        Entity existingAccount = dataSource.getEntity(accountID, ACCOUNTS_TABLE_NAME);
        //retry
        while(existingAccount != null) {
            accountID = randomAccountId();
            existingAccount = dataSource.getEntity(accountID, ACCOUNTS_TABLE_NAME);
        }
        return accountID;
    }

    @Override
    public boolean deposit(String accountNumber, BigDecimal amount) {
        Account account = getAccount(accountNumber);
        account.deposit(amount);
        LOGGER.info(String.format("Account[%s] was added deposit with amount of %s %s",
                accountNumber, amount, account.getCurrency()));
        return true;
    }

    @Override
    public boolean withdraw(String accountNumber, BigDecimal amount) {
        Account account = getAccount(accountNumber);
        account.withdraw(amount);
        LOGGER.info(String.format("Account[%s] was withdrawn with amount of %s %s",
                accountNumber, amount, account.getCurrency()));
        return true;
    }
}
