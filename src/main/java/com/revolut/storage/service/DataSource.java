package com.revolut.storage.service;

import com.revolut.model.Entity;

public interface DataSource<T extends Entity> {
    public T getEntity(String uniqueKey, String tableName);
    public T addEntity(T entity);
    public Iterable<T> getEntities(String tableName);
}
