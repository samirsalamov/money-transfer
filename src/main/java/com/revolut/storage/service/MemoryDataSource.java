package com.revolut.storage.service;

import com.revolut.model.Entity;

import javax.inject.Singleton;
import javax.ws.rs.Path;
import java.util.*;

public class MemoryDataSource<T extends Entity> implements DataSource<T> {

    private Map<String, Map<String, T>> storage = new LinkedHashMap<>();

    @Override
    public T getEntity(String uniqueKey, String tableName) {
        T latest = null;
        if (storage.containsKey(tableName)) {
            latest = storage.get(tableName).get(uniqueKey);
        }
        return latest;
    }

    @Override
    public T addEntity(T entity) {
        if (!storage.containsKey(entity.tableName())) {
            Map<String, T> entities = new HashMap<>();
            entities.put(entity.uniqueKey(), entity);
            storage.put(entity.tableName(), entities);
        } else {
            storage.get(entity.tableName()).put(entity.uniqueKey(), entity);
        }
        return entity;
    }

    @Override
    public Collection<T> getEntities(String tableName) {
        if (!storage.containsKey(tableName)) {
            return Collections.EMPTY_LIST;
        }
        return storage.get(tableName).values();
    }
}
