package com.revolut.model;

import lombok.*;
import static com.revolut.utils.AppVariables.RATES_TABLE_NAME;

@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Rate implements Entity {

    private Currency from;
    private Currency to;
    private double rate;

    @Override
    public String uniqueKey() {
        return from + "-" + to;
    }

    @Override
    public String tableName() {
        return RATES_TABLE_NAME;
    }
}
