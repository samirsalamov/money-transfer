package com.revolut.model;

public interface Entity {
    public String uniqueKey();
    public String tableName();
}
