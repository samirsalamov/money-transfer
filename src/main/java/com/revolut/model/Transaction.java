package com.revolut.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Date;

import static com.revolut.utils.AppVariables.TRANSACTIONS_TABLE_NAME;

@Getter
@Setter
@Builder
@ToString
public class Transaction implements Entity {

    private String transactionId;
    private String sourceAccount;
    private BigDecimal withdrawAmountFromSourceAccount;
    private double rateForSourceAccount;
    private String destinationAccount;
    private BigDecimal depositAmountToDestinationAccount;
    private double rateForDestinationAccount;
    private Currency currency;

    @Builder.Default
    private Date transactionTime = new Date();
    private TransactionStatus status;

    @Override
    public String uniqueKey() {
        return transactionId;
    }

    @Override
    public String tableName() {
        return TRANSACTIONS_TABLE_NAME;
    }
}
