package com.revolut.model;

import lombok.*;
import com.revolut.exception.MoneyLeftOverException;

import java.math.BigDecimal;

import static com.revolut.utils.AppVariables.ACCOUNTS_TABLE_NAME;

@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public final class Account implements Entity {

    private String accountNumber;
    private String owner;
    private Currency currency;
    @Builder.Default
    private volatile BigDecimal balance = BigDecimal.ZERO;

    public synchronized void deposit(BigDecimal amount) {
        balance = balance.add(amount);
    }

    public synchronized void withdraw(BigDecimal amount) {
        if (balance.compareTo(amount) >= 0) {
            balance = balance.subtract(amount);
        } else {
            throw new MoneyLeftOverException(
                    String.format("Account['%s'] is not left enough money to withdraw %s amount!",
                            accountNumber, amount));
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        if (!accountNumber.equals(account.accountNumber)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return accountNumber.hashCode();
    }

    @Override
    public String uniqueKey() {
        return accountNumber;
    }

    @Override
    public String tableName() {
        return ACCOUNTS_TABLE_NAME;
    }
}
