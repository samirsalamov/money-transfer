package com.revolut.model;

import lombok.*;

import java.math.BigDecimal;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TransferDTO {
    private String sourceAccount;
    private String destinationAccount;
    private BigDecimal amount;
    private Currency currency;
}
