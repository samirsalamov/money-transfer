package com.revolut.model;

public enum TransactionStatus {
    PENDING,
    DECLINED,
    COMPLETED;
}
