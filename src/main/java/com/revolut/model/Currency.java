package com.revolut.model;

public enum Currency {
   USD,
   EURO,
   PLN,
   AZN
}
