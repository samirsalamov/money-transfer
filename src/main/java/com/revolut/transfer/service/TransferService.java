package com.revolut.transfer.service;

import com.revolut.model.Transaction;
import com.revolut.model.TransferDTO;

public interface TransferService {
    public Transaction getTransaction(String id);
    public boolean transfer(TransferDTO transferDto);
}
