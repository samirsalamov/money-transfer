package com.revolut.transfer.service;

import com.revolut.account.service.AccountService;
import com.revolut.exception.AccountNotFoundException;
import com.revolut.exception.MoneyLeftOverException;
import com.revolut.exchange.service.ExchangeService;
import com.revolut.model.*;
import org.apache.log4j.Logger;
import com.revolut.storage.service.DataSource;
import javax.inject.Inject;
import java.math.BigDecimal;

import static com.revolut.utils.IDGenerator.TransactionIDGenerator.*;
import static com.revolut.utils.AppVariables.TRANSACTIONS_TABLE_NAME;

public class TransferServiceImpl implements TransferService {

    private static final Logger LOGGER = Logger.getLogger(TransferServiceImpl.class.getName());

    private AccountService accountService;
    private ExchangeService exchangeService;
    private DataSource dataSource;

    @Inject
    public TransferServiceImpl(
            AccountService accountService, ExchangeService exchangeService, DataSource dataSource) {
        this.accountService = accountService;
        this.exchangeService = exchangeService;
        this.dataSource = dataSource;
    }

    @Override
    public Transaction getTransaction(String id) {
        Entity entity = dataSource.getEntity(id, TRANSACTIONS_TABLE_NAME);
        if (entity == null) {
            return null;
        }
        return (Transaction) entity;
    }

    @Override
    public boolean transfer(TransferDTO transferDto) {
        try {
            Account sourceAccount = accountService.getAccount(transferDto.getSourceAccount());
            Account destinationAccount = accountService.getAccount(transferDto.getDestinationAccount());

            Rate rateForSourceAccount = exchangeService.getRate(transferDto.getCurrency() + "-" + sourceAccount.getCurrency());
            BigDecimal withdrawAmount = calculateAmountConsideringRate(transferDto.getAmount(), rateForSourceAccount);

            Rate rateForDestinationAccount = exchangeService.getRate(transferDto.getCurrency() + "-" + destinationAccount.getCurrency());
            BigDecimal depositAmount = calculateAmountConsideringRate(transferDto.getAmount(), rateForDestinationAccount);

            Transaction transaction = Transaction.builder()
                    .transactionId(getUniqueTransactionId())
                    .sourceAccount(transferDto.getSourceAccount())
                    .withdrawAmountFromSourceAccount(withdrawAmount)
                    .rateForSourceAccount(rateForSourceAccount == null ? 1 : rateForSourceAccount.getRate())
                    .destinationAccount(transferDto.getDestinationAccount())
                    .depositAmountToDestinationAccount(depositAmount)
                    .rateForDestinationAccount(rateForDestinationAccount == null ? 1 : rateForDestinationAccount.getRate())
                    .currency(transferDto.getCurrency())
                    .build();

            transferWithinSameTransaction(sourceAccount, destinationAccount, transaction);

            transaction.setStatus(TransactionStatus.COMPLETED);
            logTransaction(transaction);

        } catch (AccountNotFoundException | MoneyLeftOverException ex) {
            throw ex;
        }
        return true;
    }

    private BigDecimal calculateAmountConsideringRate(BigDecimal amount, Rate rate) {
        if (rate == null) {
            return amount;
        } else {
            return amount.multiply(BigDecimal.valueOf(rate.getRate()));
        }
    }

    private void transferWithinSameTransaction(Account sourceAccount, Account destinationAccount, Transaction transaction) {
        try {
            sourceAccount.withdraw(transaction.getWithdrawAmountFromSourceAccount());
            try {
                destinationAccount.deposit(transaction.getDepositAmountToDestinationAccount());
            } catch (RuntimeException ex) {
                try {
                    sourceAccount.deposit(transaction.getWithdrawAmountFromSourceAccount());
                } catch (RuntimeException e) {
                    transaction.setStatus(TransactionStatus.PENDING);
                    logTransaction(transaction);
                }
                throw ex;
            }
        } catch (RuntimeException ex) {
            transaction.setStatus(TransactionStatus.DECLINED);
            logTransaction(transaction);
            throw ex;
        }
    }

    private void logTransaction(Transaction transaction) {
        dataSource.addEntity(transaction);
        LOGGER.warn(transaction);
    }

    private String getUniqueTransactionId() {
        String id = randomTransactionId();
        Transaction existingTransaction = getTransaction(id);
        //retry
        while(existingTransaction != null) {
            id = randomTransactionId();
            existingTransaction = getTransaction(id);
        }
        return id;
    }
}
