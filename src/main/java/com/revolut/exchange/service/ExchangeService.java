package com.revolut.exchange.service;

import com.revolut.model.Rate;

import java.util.List;

public interface ExchangeService {
    public Rate getRate(String uniqueKey);
    public List<Rate> getRates();
    public Rate addRate(Rate rate);
}
