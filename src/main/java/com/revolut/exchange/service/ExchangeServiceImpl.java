package com.revolut.exchange.service;

import com.revolut.model.Entity;
import com.revolut.model.Rate;
import com.revolut.storage.service.DataSource;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import static com.revolut.utils.AppVariables.RATES_TABLE_NAME;

public class ExchangeServiceImpl implements ExchangeService {

    private static final Logger LOGGER = Logger.getLogger(ExchangeServiceImpl.class.getName());

    private DataSource dataSource;

    @Inject
    public ExchangeServiceImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Rate getRate(String uniqueKey) {
        Entity entity = dataSource.getEntity(uniqueKey, RATES_TABLE_NAME);
        if (entity == null) {
            return null;
        }
        return (Rate) entity;
    }

    @Override
    public List<Rate> getRates() {
        Iterable<Rate> entities = dataSource.getEntities(RATES_TABLE_NAME);
        List<Rate> rates = new ArrayList<>();
        entities.forEach(rates::add);
        return rates;
    }

    @Override
    public Rate addRate(Rate rate) {
        rate = (Rate) dataSource.addEntity(rate);
        LOGGER.info(String.format("Exchange rate was updated %s", rate));
        return rate;
    }
}
