# Bank Money Transfer

money-transfer is the system which allows you to transfer given amount of money between accounts. Application is very simple and supports 3 main services and provides REST for each one:

- **Account service**
- **Exchange service**
- **Transfer service**

When system starts up every-time in-memory data structure store (standard Java Map used) is being created. However persistence could be achieved by usage of Redis. Here for simplicity is chosen standard Java Map. In order to use all possible operations will be needed input data. Before using system new accounts should be created which each one identifies with unique account number. Using this account number could be done deposit, withdraw transactions over according Account. Account numbers could be used for transferring given amount money between accounts as well. Also note that Account object is unique in the system and only one instance of new created Account is being stored. And deposit and withdraw methods of Account object are safe in concurrency environment. Never race condition will happen. System considers exchanging rates. Currently is available only below given currencies:
                    
    EURO, USD, PLN, AZN    

Rates between currencies should be added by user. Rates will be used to calculate exact amount during money transfers between accounts. If exchange rate will be not found during money transfer between account then unit rate will be used.
 
However by applying Agile methodology, system could be improved by several features listed below:

- Memory data source could be changed to Database (would be better to control transactions and deal with fail-overs)
- Micro-service architecture could be used to deal better with fail-overs
- Account Service could be extended with block operation over accounts
- Transfer money between two different bank accounts could be charged predefined fees.
- Standard validations for user input data will eliminate any risk of frauds.
- Authentication (Two-factor authentication) and Authorization is very important in such systems. 
- Testing a REST API with live Integration Tests (with a JSON payload)
 
# Account service

Corresponding link: ACCOUNT_URL = [http://{host}:{port}/api/accounts](http://%7bhost%7d:%7bport%7d/api/accounts)
Available operations: 

- Read
- Create
-  Deposit
-  Withdraw.

Read operation: ACCOUNT_URL/{accountNumber}, Request type GET

Create operation: ACCOUNT_URL/, Request type POST
Body request is Json:
    
            {
                "owner": "Samir Salamov",
                "currency": "USD"
            }

Deposit operation: ACCOUNT_URL/deposit, Request type POST
Body request is application/x-www-form-urlencoded and params are:

            "accountNumber": "RVL0000000000000"
            "amount": 100
            
Withdraw operation: ACCOUNT_URL/withdraw, Request type POST
Body request is application/x-www-form-urlencoded and params are:
            
            "accountNumber": "RVL0000000000000"
            "amount": 100

# Exchange service

Corresponding link: EXCHANGE_URL = [http://{host}:{port}/api/exchanges](http://%7bhost%7d:%7bport%7d/api/exchanges)
Fetch all rates: EXCHANGE_URL/, Request type GET, Response type List<Rate> is in Json format.
Create operation: EXCHANGE_URL/, Request type POST
Body request is Json:
            
            {
                "from": "USD"
                "to": "PLN"
                "rate": 3.75
            }   

# Transfer service

Corresponding link: TRANSFER_URL = [http://{host}:{port}/api/transfer](http://%7bhost%7d:%7bport%7d/api/transfer)
Transfer operation: EXCHANGE_URL/, Request type POST
Body request is Json:
            
	        {
                "sourceAccount": "RVL60796844846207",
                "destinationAccount": "RVL47752462566612",
                "amount": 20,
                "currency": "EURO",
            }

# Application Requirements
            
            Java 8+
            Maven 3+

# Application Build and Run

Clone application by below command:

        git clone https://samirsalamov@bitbucket.org/samirsalamov/money-transfer.git

As project is maven application it could be built by simple below command under root folder:

        mvn clean install
 
 Build will be run along with all unit tests. In final executable jar file will consist Embedded Jetty Server and REST with Java (JAX-RS) using Jersey libraries. In order to run application below command is being used under target folder:
 
        java -jar money-transfer-1.0-SNAPSHOT.jar
  
  Application will be available over REST in https://localhost:8000/api root link. If you have already Postman is installed import predefined collection requests by this link: https://bitbucket.org/samirsalamov/money-transfer/src/master/revolute-banking-api.postman_collection.json